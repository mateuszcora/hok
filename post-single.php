					<div class="col">
						<div class="post-image">
							<a href="#" alt="image">
								<?php if(has_post_thumbnail()){?>
								<img src="<?php the_post_thumbnail_url( 'full' ); ?>" class="img-responsive center-block">
								<?php } ?>
							</a>						
						</div>
						<div class="post-text post-text-single">
							<?php the_content() ?>
						</div>
					</div>