<?php get_header();?>
		<div class="site-content post-single wrapper-content container-fluid">
			<div class="row">
			<?php
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post(); 
							get_template_part('post', 'single');
				} // end while
			} // end if
			?>
			</div><!--row-->
		</div><!--wrapper-content-->
<?php get_footer();?>