<?php get_header();?>
	<div class="site-content">
		<div class="page-wrapper">
			<div class="another-fucking-page-wrapper">
				<?php
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post(); 
							?> 
							<h1 class="page-title">
								<?php the_title(); ?>
							</h1>
							<div class="page-content">
								<?php the_content(); ?>
							</div>
						
					<?php	
					} // end while
				} // end if
				?>
			</div>
		</div>
	</div>
<?php get_footer();?>