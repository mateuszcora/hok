<?php
/*
 * @package WordPress
 * @subpackage HOK
 * @since 0.1
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head();?>
</head>

<body <?php echo body_class();?>>
	<nav class="navbar navbar-fixed-top navbar-custom-top <?php if(is_admin_bar_showing()) echo("admin-bar-space");?>">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed toggle-button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-left" href="<?php echo(get_bloginfo("url"));?>">
		  </a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<?php

			/*main menu*/
			wp_nav_menu( array(
				'theme_location' => 'Main',
				'menu_class' => 'nav navbar-nav main-menu',
				));
		
			/*social menu*/
			echo hok_menu('Social', "nav navbar-nav navbar-right"); ?>

		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>