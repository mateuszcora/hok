<?php
/*theme support*/


function custom_theme_features()  {
	add_theme_support( 'custom-header', array(
		'default-image'          => '',
		'width'                  => 0,
		'height'                 => 0,
		'flex-width'             => true,
		'flex-height'            => false,
		'uploads'                => true,
		'random-default'         => false,
		'header-text'            => false,
		'default-text-color'     => '',
		'wp-head-callback'       => '',
		'admin-head-callback'    => '',
		'admin-preview-callback' => '',
		'video'                  => true,
		'video-active-callback'  => '',
	) );
	add_theme_support( 'post-thumbnails');
	/*add_theme_support( 'custom-background', array(
		'default-color' => '000000',
		'default-image' => '%1$s/images/background.png',
		'wp-head-callback'       => 'background_image_callback',
	) );*/
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );
}
add_action( 'after_setup_theme', 'custom_theme_features' );


/*menus (duh)*/
function register_hok_menus() {
  register_nav_menus(
    array(
      'Main' => __( 'Main' ),
      'Social' => __( 'Social' ),
	  'Footer' => __( 'Footer' )
    )
  );
}
add_action( 'init', 'register_hok_menus' );



/* Styles */
wp_enqueue_style('font-sans-serif', 'https://fonts.googleapis.com/css?family=Quicksand&amp;subset=latin-ext');
wp_enqueue_style('font-serif', 'https://fonts.googleapis.com/css?family=Lustria');
wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
wp_enqueue_style('main', get_bloginfo('template_directory') .'/style.css');
wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

/* Javascript */
wp_enqueue_script('jQuery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js');
wp_enqueue_script('bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
wp_enqueue_script('masonry', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js');
wp_enqueue_script('main', get_bloginfo('template_directory') .'/scripts.js');

/*various functions*/
	function background_image_callback(){
	
	}
	function hok_menu($menu_id, $list_class){
		$html='<ul class="' . $list_class . '"/>';
		$menu_items = wp_get_nav_menu_items($menu_id);
		foreach ($menu_items as $item){
			$html .= '<li><a href="' . $item->url . '" class="';
			foreach($item->classes as $class)
				$html .= " ". $class;
			$html .='"></a></li>';
		}
		$html .= "</ul>";
		return $html;
	}
?>