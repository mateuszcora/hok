	<div class="container-fluid footer">
		<div>
			<div class="row">
				<div class="col-md-2 col-md-offset-3 footer-element footer-menu">
					<?php wp_nav_menu(array(
						'menu' => 'Footer',
						"menu_class" => 'list-unstyled',
						'items-wrap' => 'test'
					)); ?>
				</div>
				<div class="col-md-2 col-md-offset-2 footer-element">
					<?php echo hok_menu('Social', 'list-inline list-unstyled');?>
				</div>
			</div>
			<div class="notice">
				Powered by WordPress </br>
				Copyright &#169; Humans of Krakow
			</div>
		</div>
	</div>
<?php wp_footer(); ?>
</body>