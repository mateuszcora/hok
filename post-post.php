					<div class="col-lg-3 col-md-4 col-sm-6 grid-item">
						<div class="post-image">
							<a href="<?php the_permalink(); ?>" alt="image">
								<?php if(has_post_thumbnail()){?>
								<img src="<?php the_post_thumbnail_url( 'medium_large' ); ?>" class="img-responsive">
								<?php } ?>
							</a>						
						</div>
						<div class="post-text">
							<?php the_excerpt() ?>
						</div>
					</div>