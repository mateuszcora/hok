<?php
/**
 * @package WordPress
 * @subpackage HOK
 * @since 0.1
*/
	get_header(); 

	/*header area*/
	if(is_front_page()){
		?>
	<div class="header <?php if (is_admin_bar_showing()) echo " header-admin-bar-space"; ?>">
		<img src="<?php echo get_header_image();?>" alt="Humans of Krak�w" class="header-img">
		<div class="branding-wrapper <?php if (is_paged()) echo "branding-wrapper-short"; ?>">
			<div class="branding">
				<p class="branding-title">
					<?php echo (get_bloginfo('name'));?>
				</p>
				<p class="branding-tagline">
					<?php echo (get_bloginfo('description'));?>
				</p>
			</div>
		</div>
	</div>
	<?php }
	?>
	<div class=" site-content 
				<?php 
				if(!is_single() && !is_paged()) 
					echo (' header-space');
				if(is_paged())
					echo(' header-space-short');
					
					?>
				
				">
		<div class="wrapper-content container-fluid">
			<div class="row">
				<div class="grid">
				<?php
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post(); 
				
							get_template_part('post', 'post');
					
					} // end while
				} // end if
				?>
				</div> <!--grid-->
			</div><!--row-->

		</div><!--wrapper-content-->
		<div class="container-fluid pagination-wrapper">
			<div class="row pagination">
				<div class="col-md-5 ">
					<?php previous_posts_link(); ?>
				</div>
				<div class="col-md-5 col-md-offset-2">
					<?php next_posts_link(); ?>
				</div>
			</div>
		</div>
	</div><!--site content-->


<?php
get_footer();
?>